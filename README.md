## Branches

***main*** : version stable

***develop*** : version proche de la main avec quelques features en attentes de validation

IMPORTANT : Créer une branche pour chaque feature sur laquelle vous travaillez

ex : *feature/nav-menu*

---

## Pipelines

La pipeline ***sast*** vérifie que les règles de sécurité et de propreté sont vérifiées, elle n’est pas blocante.

La pipeline ***build*** vérifie que le build React est fonctionnel, elle est bloquante.

---

## Commandes

- **Récupérer la derniere version du repo**
    
     `git pull origin develop`
    

- Lancer l’application
    
    `npm run start`
    

- **Envoyer une nouvelle version**
    
    `git add .` : Ajoute tous vos fichiers au changement
    
    `git commit -m “le thème de votre update”` : Donne un nom au changement
    
    `git pull origin develop` : Récupère la dernière version pour être à jour
    
    `git push originlenomdevotrebranche` : Push votre changement sur votre branche
    

---