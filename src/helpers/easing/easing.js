export const easeInQuad = (t, b, c, d) => {
  return c * (t /= d) * t + b;
};

export const linear = (t, b, c, d) => {
  return (c * t) / d + b;
};
export const easeOutQuint = (t, b, c, d) => {
  return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
};
